const http = require('http');
const server = http.createServer(router);
const io = require('socket.io')(server);

function router(req, res) {
  switch(req.method) {
    case 'GET':
      return res.end();
    case 'POST':
      return getTicket(req, res).then((data) => {
        console.log(Date.now(), 'updates', JSON.stringify(data));
        io.sockets.emit('updates', data);
        res.end();
      }).catch((er) => { 
        res.statusCode = 500;
        res.end();
      });
  }
}

function getTicket(req, res, data='') {
  return new Promise((resolve, reject) => {
    try {
      req.on('data', (chunk) => { data += chunk; });
      req.on('end', () => { resolve(JSON.parse(data)); });
      req.on('error', reject);
    } catch (er) { 
      reject(er);
    }
  });
}

server.listen(process.env.PORT || 3001);
