# README #

This is the socket server for the [mc-web-server](https://bitbucket.org/_s84/mc-web-server).

### How do I get set up? ###

    // If you need help getting node.js nvm is nice!
    curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.31.1/install.sh | bash
    // get Node.js v6.1.0
    nvm install v6.1.0
    nvm use v6.1.0
    git clone https://_s84@bitbucket.org/_s84/mc-socket-server.git
    cd mc-socket-server
    npm install
    node web.js
    